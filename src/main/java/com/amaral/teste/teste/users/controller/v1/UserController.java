package com.amaral.teste.teste.users.controller.v1;

import com.amaral.teste.teste.users.dto.UserDTO;
import com.amaral.teste.teste.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(service.getall());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getItem(@PathVariable("id") Long id){
        return ResponseEntity.ok(service.getItem(id));
    }

    @PostMapping
    public ResponseEntity<?> insert( @Validated @RequestBody UserDTO userDto){
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/products/{id}")
                .build(service.insert(userDto));

        return ResponseEntity.created(location).build();
    }
}
