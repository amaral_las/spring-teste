package com.amaral.teste.teste.users.controller.v1.advice;

import com.amaral.teste.teste.users.exception.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

    @ExceptionHandler(ResourceNotFoundException.class)
    public HttpEntity<StandardError> resourceNotFoundException(final ResourceNotFoundException e,
                                                               final HttpServletRequest httpRequest){

        HttpStatus status = HttpStatus.NOT_FOUND;

        return ResponseEntity
                .status(status)
                .body(newStandardError(
                        status,
                        "Resource not found.",
                        e,
                        httpRequest));

    }

    private StandardError newStandardError(HttpStatus status, String titleError, RuntimeException e, HttpServletRequest request){
        return new StandardError(
                formatter.format(System.currentTimeMillis()),
                e.getMessage(),
                request.getRequestURI());
    }
}
