package com.amaral.teste.teste.users.controller.v1.advice;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StandardError {
        private String timestamp;
        private String message;
        private String path;
}
