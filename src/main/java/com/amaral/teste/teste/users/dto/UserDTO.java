package com.amaral.teste.teste.users.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id ;
    private String nome;
    private String password;
}
