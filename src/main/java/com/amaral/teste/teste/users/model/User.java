package com.amaral.teste.teste.users.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder
@Data
@Table
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @GeneratedValue
    @Id
    private Long id;

    @NotNull
    public String nome;

    @NotNull
    private String pasword;
}
