package com.amaral.teste.teste.users.model_mapper;

import com.amaral.teste.teste.users.dto.UserDTO;
import com.amaral.teste.teste.users.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface UserModelMapper {

    User toUser(UserDTO userDto);

    @Mapping(target = "password", ignore = true)
    UserDTO toUserDTO(User user);

    List<UserDTO> toUserDTOs(List<User> users);
}
