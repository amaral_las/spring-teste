package com.amaral.teste.teste.users.service;

import com.amaral.teste.teste.users.dto.UserDTO;
import com.amaral.teste.teste.users.exception.ResourceNotFoundException;
import com.amaral.teste.teste.users.model.User;
import com.amaral.teste.teste.users.model_mapper.UserModelMapper;
import com.amaral.teste.teste.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;
    private final UserModelMapper mapper;

    public List<UserDTO> getall(){
        return mapper.toUserDTOs(repository.findAll());
    }

    public UserDTO getItem(Long id) {
        return mapper.toUserDTO(repository.findById(id).orElseThrow(ResourceNotFoundException::new));
    }

    public Long insert(UserDTO userDto){
        return repository.save(mapper.toUser(userDto)).getId();
    }
}
